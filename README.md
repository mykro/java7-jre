# mykro/java7-jre
A build on top of Debian 8

## Image Sizes 
[![](https://badge.imagelayers.io/mykro/java7-jre:7u80.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u80) 7u80, Latest
[![](https://badge.imagelayers.io/mykro/java7-jre:7u79.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u79) 7u79
[![](https://badge.imagelayers.io/mykro/java7-jre:7u76.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u76) 7u76
[![](https://badge.imagelayers.io/mykro/java7-jre:7u75.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u75) 7u75
[![](https://badge.imagelayers.io/mykro/java7-jre:7u72.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u72) 7u72
[![](https://badge.imagelayers.io/mykro/java7-jre:7u71.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u71) 7u71
[![](https://badge.imagelayers.io/mykro/java7-jre:7u67.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u67) 7u67
[![](https://badge.imagelayers.io/mykro/java7-jre:7u65.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u65) 7u65
[![](https://badge.imagelayers.io/mykro/java7-jre:7u60.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u60) 7u60
[![](https://badge.imagelayers.io/mykro/java7-jre:7u55.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u55) 7u55
[![](https://badge.imagelayers.io/mykro/java7-jre:7u51.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u51) 7u51
[![](https://badge.imagelayers.io/mykro/java7-jre:7u45.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u45) 7u45
[![](https://badge.imagelayers.io/mykro/java7-jre:7u40.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u40) 7u40
[![](https://badge.imagelayers.io/mykro/java7-jre:7u21.svg)](https://imagelayers.io/?images=mykro/java7-jre:7u21) 7u21

## Using this Image
Primarily, this image has been built to be a base for any Java Application that requires the Server JRE. It also strips out as much as possible to keep the image as lean as it can be.

```Dockerfile
FROM mykro/java7-jre:latest
MAINTAINER me <me@me.com>

# Build your java app container here

```
